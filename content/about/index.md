# About

## What this Blog is About

This blog is operated by Tocco's DevOps Team. It contains information about important
changes or interesting topics. This blog is technical and geared towards people working
at Tocco. Some content may still be of interest for the public, however.

Source can be found at https://gitlab.com/toccoag/devops-blog.

## Editor's Note

One of my goals for 2022 is to improve information flow from the DevOps team to all other,
technical teams within Tocco. This blog is the means by which this information will
be provided.

For the time being this should be seen as an experiment. My roadmap looks like this
currently and is subject to change:

* Until Autumn 2022: write the first few posts
* Autumn 2022: limited survey about content and means of presentation
* End of 2022: decide how to proceed

-- Peter
