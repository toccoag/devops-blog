# DevOps Blog

Blog located at https://devops-blog.tocco.ch.

See also [about page](https://devops-blog.tocco.ch/about/)

## Build Locally

* `apt install hugo`
* `hugo server --watch` and follow instructions

## Add Post

Text only post:

```
hugo new content/posts/$(date +%F)-<INSERT_NAME_HERE>.md
```

Post containing resources (e.g images):

```
hugo new content/posts/$(date +%F)-<INSERT_NAME_HERE>/index.md
```

Place resources in the created folder.

yyyy-mm-dd date prefix used to ensure proper ordering.

**Important:**

If the date (`date = …`) in the file is in the future, the post won't
appear in the list and you'll have to rerun GitLab CI after the date
to have it published.
