---
title: "Fallback Sender Domain, tocco-mailing-service.ch"
date: 2022-02-17T15:05:25+01:00
---

# For What is the Domain?

The domain may be used as sender domain of mails for any installation.

There are two use cases:

1. The domain can be used while the customer is in the process of
   configuring DKIM and SPF for their own domains. This avoids any
   delays waiting for the domains to be ready.
2. For customers that do not wish to use their own domain, it can be
   used permanently as sender domain.


# How Does it Work?

When an installation is created, and the customer domain or domains are not ready yet, simply
omit the mail setting in [config.yml]:

```yaml
example:   # <-- customer
  # mail_sender_default:          #
  # mail_sender_noreply:          # Omit this settings entirely
  # mail_domains:                 #
  # - example.com                 #
  installations:
    example:
      # …
    exampletest:
      # …
```

If omitted, the following default settings take precedence:

```yaml
example:   # <-- customer
  mail_sender_default: noreply@tocco-mailing-service.ch       #
  mail_sender_noreply: noreply@tocco-mailing-service.ch       # Default values
  mail_domains:                                               #
  - example.net                                               # Do not set in config.yml explicitly
  installations:
    example:
      # …
    exampletest:
      # …
```

Documentation has been adjusted accordingly.


# When are the *default* and *noreply* Senders Used?

`mail_sender_default` and `mail_sender_noreply` are used when …:

1. … a mail has a sender address not listed in `mail_domains` [^1]. For instance,
   we can't send a mail for a Gmail address like *user\@gmail.com*. Instead, we
   send it as *noreply\@tocco-mailing-service.ch* (or any other address set as
   `mail_sender_noreply`) and set *user\@gmail.com* as reply-to address. This way,
   replies end up in the right place while we are still allowed to send
   mails.

2. … a mail is sent by the system rather than a specific user. This
   could be a password reset mail or a reminder mail sent via batch
   job.

As usual, reality is a bit more complicated. Whenever a mail is sent on
behalf of a [business unit] (BU), *Default e-mail sender* and *No reply
e-mail sender* on the BU, if set, are used instead:

![Business Unit](bu.png "Business Unit settings")

This also applies to mails sent manually, as when you're logged in,
you're working in a selected BU:

![BU Selection](bu_selection.png "Business Unit selection")

# See Also

* [Configure Default Sender Addresses]
* [Configure Email Sender Domains]
* [Default Domain tocco-mailing-service.ch]


[Default Domain tocco-mailing-service.ch]: https://docs.tocco.ch/devops/app_management/ansible.html#default-domain-tocco-mailing-service-ch
[Configure Default Sender Addresses]: https://docs.tocco.ch/devops/app_management/ansible.html#configure-default-sender-addresses
[Configure Email Sender Domains]: https://docs.tocco.ch/devops/app_management/ansible.html#configure-email-sender-domains
[config.yml]: https://docs.tocco.ch/glossary.html#term-config.yml
[business unit]: https://master.tocco.ch/tocco/e/Business_unit/list
[^1]: See [Configure Default Sender Addresses]
